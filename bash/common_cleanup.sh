#!/bin/bash
set -e

# Cleanup temporary packages and files:
rm -rf /var/lib/apt/lists/* /tmp/*
