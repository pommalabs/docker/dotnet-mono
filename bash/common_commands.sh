#!/bin/bash
set -e

export DEBIAN_FRONTEND="noninteractive"
export SAFE_CURL="curl --silent --show-error --fail"
export SLIM_INSTALL="apt-get install -y -qq --no-install-recommends"
