#!/bin/bash
set -e

source /opt/bash/common_commands.sh

# Setup APT packages:
$SLIM_INSTALL apt-transport-https gnupg dirmngr

# Install Mono:
export GNUPGHOME="$(mktemp -d)"
gpg --batch --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
gpg --batch --export --armor 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF > /etc/apt/trusted.gpg.d/mono.gpg.asc
gpgconf --kill all
rm -rf "$GNUPGHOME"
echo "deb https://download.mono-project.com/repo/debian stable-$MONO_REPO_DISTRO/snapshots/$MONO_VERSION main" | tee /etc/apt/sources.list.d/mono-official-stable.list
apt-get update -qq
$SLIM_INSTALL mono-complete
mono --version && mcs --about

# Cleanup temporary packages and files:
apt-get purge -y -qq --auto-remove gnupg dirmngr
