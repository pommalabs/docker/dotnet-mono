stages:
  - publish
  - test

publish-on-container-registry:
  stage: publish
  image:
    name: docker:latest
    pull_policy: if-not-present
  services:
    - name: docker:dind
      pull_policy: if-not-present
      command: ["--experimental", "--mtu=1450"]
  variables:
    IMAGE_AUTHOR: "PommaLabs Team and Contributors <hello@pommalabs.xyz>"
    IMAGE_TITLE: "Docker image with .NET and Mono, based on Debian Linux"
    IMAGE_DESCRIPTION: "Docker image with .NET and Mono, based on Debian Linux"
    IMAGE_VENDOR: "PommaLabs"
    # When using dind service, you must instruct Docker
    # to talk with the daemon started inside of the service.
    # The daemon is available with a network connection
    # instead of the default `/var/run/docker.sock` socket.
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates.
    # Docker creates them automatically on boot,
    # and creates `/certs/client` to share between the service
    # and job container, thanks to volume mount from config.
    DOCKER_TLS_CERTDIR: "/certs"
    # These are usually specified by the entrypoint, however the
    # Kubernetes executor doesn't run entrypoints:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  before_script:
    - apk add --no-cache git
    - git --version
    - until docker info; do sleep 1; done
    # Tag of "tonistiigi/binfmt" docker image has been fixed to "qemu-v7.0.0-28"
    # in order to address following bug: https://github.com/tonistiigi/binfmt/issues/240.
    # See also following comment: https://github.com/tonistiigi/binfmt/issues/240#issuecomment-2679638769.
    - docker run --privileged --rm tonistiigi/binfmt:qemu-v7.0.0-28 --uninstall arm64
    - docker run --privileged --rm tonistiigi/binfmt:qemu-v7.0.0-28 --install arm64
    - docker context create multi-arch-context
    - docker buildx create --use multi-arch-context
    - docker buildx inspect --bootstrap
  script:
    - docker login -u $CONTAINER_REGISTRY_USER -p $CONTAINER_REGISTRY_PASSWORD $CONTAINER_REGISTRY_HOST
    - |
      [[ $CI_COMMIT_REF_NAME = "preview" ]] && suffix="-preview" || suffix=""
      while IFS=\| read -r path tag platform
      do
        tag="$tag$suffix"
        [[ $tag = "latest-preview" ]] && tag="preview" || tag=$tag
        echo $path "-->" $tag
        tag="$CONTAINER_REGISTRY_HOST/$CONTAINER_REGISTRY_ORGANIZATION/$CONTAINER_REGISTRY_REPOSITORY:$tag"
        # https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys
        docker buildx build --sbom=true -t $tag --push \
          --build-arg SUFFIX="$suffix" \
          --label "maintainer=$IMAGE_AUTHOR" \
          --label "org.opencontainers.image.authors=$IMAGE_AUTHOR" \
          --label "org.opencontainers.image.created=$(date -Iseconds)" \
          --label "org.opencontainers.image.url=$CI_PROJECT_URL" \
          --label "org.opencontainers.image.documentation=$CI_PROJECT_URL" \
          --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
          --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
          --label "org.opencontainers.image.vendor=$IMAGE_VENDOR" \
          --label "org.opencontainers.image.title=$IMAGE_TITLE" \
          --label "description=$IMAGE_DESCRIPTION" \
          --label "org.opencontainers.image.description=$IMAGE_DESCRIPTION" \
          --platform "$platform" \
          -f $path/Dockerfile .
        docker buildx imagetools inspect $tag
      done < ".dockertags"
  only:
    - main
    - preview

build-test-libraries:
  stage: test
  image:
    name: docker:latest
    pull_policy: if-not-present
  services:
    - name: docker:dind
      pull_policy: if-not-present
  variables:
    # When using dind service, you must instruct Docker
    # to talk with the daemon started inside of the service.
    # The daemon is available with a network connection
    # instead of the default `/var/run/docker.sock` socket.
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates.
    # Docker creates them automatically on boot,
    # and creates `/certs/client` to share between the service
    # and job container, thanks to volume mount from config.
    DOCKER_TLS_CERTDIR: "/certs"
    # These are usually specified by the entrypoint, however the
    # Kubernetes executor doesn't run entrypoints:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  before_script:
    - until docker info; do sleep 1; done
  script:
    - |
      [[ $CI_COMMIT_REF_NAME = "preview" ]] && suffix="-preview" || suffix=""
      for tp in $(find $(pwd) -type f -name "test-lib.csproj")
      do
        cd "$(dirname "${tp}")"
        echo "$(pwd)"
        docker builder prune --all --force
        docker build . -f Dockerfile --build-arg SUFFIX="$suffix" -t test-lib
      done
  only:
    - main
    - preview

run-test-projects:
  stage: test
  image:
    name: docker:latest
    pull_policy: if-not-present
  services:
    - name: docker:dind
      pull_policy: if-not-present
  variables:
    # When using dind service, you must instruct Docker
    # to talk with the daemon started inside of the service.
    # The daemon is available with a network connection
    # instead of the default `/var/run/docker.sock` socket.
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates.
    # Docker creates them automatically on boot,
    # and creates `/certs/client` to share between the service
    # and job container, thanks to volume mount from config.
    DOCKER_TLS_CERTDIR: "/certs"
    # These are usually specified by the entrypoint, however the
    # Kubernetes executor doesn't run entrypoints:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  before_script:
    - until docker info; do sleep 1; done
  script:
    - |
      [[ $CI_COMMIT_REF_NAME = "preview" ]] && suffix="-preview" || suffix=""
      for tp in $(find $(pwd) -type f -name "test-prj.csproj")
      do
        cd "$(dirname "${tp}")"
        echo "$(pwd)"
        docker builder prune --all --force
        docker build . -f Dockerfile --build-arg SUFFIX="$suffix" -t test-prj
        docker run -t test-prj
      done
  only:
    - main
    - preview
